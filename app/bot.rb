class Bot
  TOKEN = '1228182676:AAETow_YfOQe2iPKma3bRcUu54vRAlXEdPg'

  def run
    Telegram::Bot::Client.run(TOKEN) do |bot|
			@ban_names = JSON(File.read('data/ban_names.json'))
      bot.listen do |message|
        chat_id = message.chat.id
          case message.text
					when '/start'
            bot.api.send_message(chat_id: chat_id, text: "Hello, #{message.from.first_name}")
          when '/ban_list'
            bot.api.send_message(chat_id: chat_id, text: "Бан лист флюидных имен:\n#{(@ban_names[chat_id] || []).map(&:capitalize).join(', ')}")
          else
            (add_name(bot, chat_id, message.text.gsub('/add_name', '').strip) && next) if (message.text || '').include?('/add_name')
            (message.text || '').split(' ').each do |text|
              delete_message(bot, message) if (@ban_names[chat_id] || []).include?(text.downcase)
            end
          end
      end
		end
	end

  def add_name(bot, chat_id, name)
    if @ban_names.keys.include?(chat_id)
      @ban_names[chat_id] << name.downcase
    else
      @ban_names[chat_id] = [name.downcase]
    end
    File.open('data/ban_names.json', 'w') { |f| f.write(JSON(@ban_names)) }
    bot.api.send_message(chat_id: chat_id, text: "Теперь бан лист выглядит так:\n#{@ban_names[chat_id].map(&:capitalize).join(', ')}")
  end

  def delete_message(bot, message)
    bot.api.delete_message(chat_id: message.chat.id, message_id: message.message_id)
    bot.api.send_message(chat_id: message.chat.id, text: "ВНИМАНИЕ, ОБЪЯВЛЯЮ!!!!!\n@#{message.from.username} ЧЕХОЛ")
  end
end
